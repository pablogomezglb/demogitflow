﻿using DemoGitflow.Core;
using DemoGitflow.Data.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DemoGitflow.UI.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IRestaurantData _restaurantData;
        private readonly ILogger<IndexModel> _logger;

        public IEnumerable<Restaurant> Restaurants { get; set; }

        public IndexModel(IRestaurantData restaurantData, ILogger<IndexModel> logger)
        {
            _restaurantData = restaurantData;
            _logger = logger;
        }

        public void OnGet()
        {
            Restaurants = _restaurantData.GetAll();
        }
    }
}

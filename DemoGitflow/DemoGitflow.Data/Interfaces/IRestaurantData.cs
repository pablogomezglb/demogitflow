﻿using DemoGitflow.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoGitflow.Data.Interfaces
{
    public interface IRestaurantData
    {
        IEnumerable<Restaurant> GetAll();
    }
}

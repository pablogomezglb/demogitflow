﻿using DemoGitflow.Core;
using DemoGitflow.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoGitflow.Data.Repository
{
    public class InMemoryCuisineData : ICuisineData
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public InMemoryCuisineData(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IEnumerable<Cuisine> GetAll()
        {
            return _applicationDbContext.Cuisines.Include("Cuisine").AsEnumerable();
        }
    }
}

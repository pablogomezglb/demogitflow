﻿using DemoGitflow.Core;
using DemoGitflow.Data.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DemoGitflow.Data.Repository
{
    public class InMemoryRestaurantData : IRestaurantData
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public InMemoryRestaurantData(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public IEnumerable<Restaurant> GetAll()
        {
            return _applicationDbContext.Restaurants.Include("Cuisine").AsEnumerable();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DemoGitflow.Core
{
    public class Restaurant
    {
        public int Id { get; set; }

        [Required, StringLength(80)]
        public string Name { get; set; }

        [Required, StringLength(255)]
        public string Location { get; set; }

        public string Address { get; set; }

        public string OpenHours { get; set; }

        public int CuisineId { get; set; }

        public Cuisine Cuisine { get; set; }
    }
}
